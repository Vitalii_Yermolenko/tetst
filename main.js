class Employee {
    constructor(name,age,salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name(){
       return this._name;
    }
    
    set name(name){
       console.log(`Name is ${this.name}`);
    }

    get age(){
        return this._age;
    }
    
    set age(age){
        console.log(`Age is ${this.age}`)
    }

    get salary(){
        return this._salary;
    }

    set salary(salary){
        console.log(`Salary is ${this.salary}`);
    }



}

class Programmer extends Employee{
    constructor(name, age, salar,lang){
        super(name , age, salar);
        this._lang = lang;
    }

    get lang(){
        return this._lang;
    }

    set lang(lang){
        console.log(`${lang}`);
    }
    get salary(){
        return this._salary*3;
    }

    description(){
        console.log(`Моє ім'я ${this._name}. Мій вік ${this._age}. Я заробляю ${this.salary}, бо знаю такі мови програмування: ${this._lang} .`);
    }
}

let firstDeveloper = new Programmer('Oleg',30, 2000 , ['js',"html"]);

console.log(firstDeveloper); 
firstDeveloper.description();

let secondDeveloper = new Programmer('Olya',25, 4000, ['js',"html","css"]);

console.log(secondDeveloper); 
secondDeveloper.description();

let thirdDeveloper = new Programmer('Ivan', 43, 5000, ["js","java","type script"]);

console.log(thirdDeveloper); 
thirdDeveloper.description();

let fourthDeveloper = new Programmer('Nadia', 19, 3000, ["js","java"]);

console.log(fourthDeveloper); 
fourthDeveloper.description();

let fifthDeveloper = new Programmer('Volodimir', 38, 7000, ["js","java","type script","html","css"]);

console.log(fifthDeveloper); 
fifthDeveloper.description();